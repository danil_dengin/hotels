pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "Hotels"
include (":app")
include(":core:network")
include(":core:utils")
include(":core:mvvm")
include(":core:ui")
include(":core:di")
include(":feature:hotel-list:impl")
include(":feature:hotel-details:api")
include(":feature:hotel-details:impl")
include(":feature:hotel-service")
include(":feature:hotel-filter:impl")
include(":feature:hotel-filter:api")
