package com.hotels.feature.filter.impl.presentation

import com.hotels.core.ui.R as CoreRes
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hotels.core.utils.delegate.unsafeLazy
import com.hotels.feature.filter.api.FILTER_BUNDLE_KEY
import com.hotels.feature.filter.api.FILTER_KEY
import com.hotels.feature.filter.api.SelectedFilter
import com.hotels.feature.filter.impl.R
import com.hotels.feature.filter.impl.databinding.FragmentHotelFilterBinding

class HotelFilterFragment : BottomSheetDialogFragment(R.layout.fragment_hotel_filter) {

    private val viewModel: HotelFilterViewModel by viewModels()

    private val binding by viewBinding(FragmentHotelFilterBinding::bind)

    private val filter by unsafeLazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arguments?.getParcelable(SELECTED_FILTER_ARG, SelectedFilter::class.java)
        } else {
            arguments?.getParcelable(SELECTED_FILTER_ARG)
        }
    }

    override fun getTheme() = CoreRes.style.AppBottomSheetDialogTheme

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkSelectedFilter()
        initRadioButtonListener()
        initApplyButtonListener()
        initResetListener()
    }

    private fun checkSelectedFilter() {
        filter?.also { selectedFilter ->
            when (selectedFilter) {
                SelectedFilter.NOTHING -> {}
                SelectedFilter.AVAILABLE_ROOM -> {
                    binding.radioButtonAvailableHotelNumber.isChecked = true
                    selectFilter(SelectedFilter.AVAILABLE_ROOM)
                }
                SelectedFilter.DISTANCE -> {
                    binding.radioButtonDistance.isChecked = true
                    selectFilter(SelectedFilter.DISTANCE)
                }
            }
        }
    }

    private fun initResetListener() {
        binding.resetTextView.setOnClickListener {
            setFragmentResult(parentFragmentManager, SelectedFilter.NOTHING)
            viewModel.applyFilter(findNavController())
        }
    }

    private fun initApplyButtonListener() {
        binding.applyButton.setOnClickListener {
            setFragmentResult(parentFragmentManager, viewModel.selectedFilter)
            viewModel.applyFilter(findNavController())
        }
    }

    private fun setFragmentResult(
        fragmentManager: FragmentManager,
        selectedFilter: SelectedFilter
    ) {
        fragmentManager.setFragmentResult(
            FILTER_KEY,
            bundleOf(FILTER_BUNDLE_KEY to selectedFilter)
        )
    }

    private fun initRadioButtonListener() {
        with(binding) {
            radioGroupHotelFilter.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.radioButtonDistance -> {
                        selectFilter(SelectedFilter.DISTANCE)
                    }
                    R.id.radioButtonAvailableHotelNumber -> {
                        selectFilter(SelectedFilter.AVAILABLE_ROOM)
                    }
                }
            }
        }
    }

    private fun selectFilter(selectedFilter: SelectedFilter) {
        binding.applyButton.isEnabled = true
        viewModel.selectedFilter = selectedFilter
    }

    companion object {
        const val SELECTED_FILTER_ARG = "selectedFilter"
    }
}