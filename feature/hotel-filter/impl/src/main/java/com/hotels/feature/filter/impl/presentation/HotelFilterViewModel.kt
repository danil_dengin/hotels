package com.hotels.feature.filter.impl.presentation

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.hotels.feature.filter.api.SelectedFilter

internal class HotelFilterViewModel : ViewModel() {

    var selectedFilter = SelectedFilter.NOTHING

    fun applyFilter(navController: NavController) {
        navController.navigateUp()
    }
}