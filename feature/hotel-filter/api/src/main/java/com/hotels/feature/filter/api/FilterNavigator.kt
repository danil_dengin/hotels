package com.hotels.feature.filter.api

import androidx.navigation.NavController

interface FilterNavigator {

    fun navigateToHotelFilter(selectedFilter: SelectedFilter, navController: NavController)
}