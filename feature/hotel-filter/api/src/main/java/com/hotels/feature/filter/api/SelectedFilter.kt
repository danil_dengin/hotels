package com.hotels.feature.filter.api

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class SelectedFilter : Parcelable {
    DISTANCE, AVAILABLE_ROOM, NOTHING
}