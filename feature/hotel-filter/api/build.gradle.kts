plugins {
    id(Plugins.androidLibrary)
    id(Plugins.jetbrainsAndroid)
    id(Plugins.kotlinParcelize)
}

applyAndroid()

dependencies {
    jetpackNavigation()
}