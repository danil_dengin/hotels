plugins {
    id(Plugins.androidLibrary)
    id(Plugins.kotlinKapt)
    id(Plugins.jetbrainsAndroid)
}

applyAndroid(useViewBinding = true)

dependencies {
    implementation(project(":core:ui"))
    implementation(project(":core:utils"))
    implementation(project(":core:network"))
    implementation(project(":core:di"))
    implementation(project(":core:mvvm"))
    implementation(project(":feature:hotel-details:api"))
    implementation(project(":feature:hotel-service"))
    retrofit()
    dagger()
    jetpackNavigation()
    implementation(Dependencies.appCompat)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.viewBindingDelegate)
    implementation(Dependencies.lifecycleViewModel)
    implementation(Dependencies.appCompat)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.material)
    implementation(Dependencies.glide)
}