package com.hotels.feature.details.impl.di

import com.hotels.core.di.AppDependencies
import com.hotels.core.ui.stringResources.StringResources
import retrofit2.Retrofit

interface HotelDetailsComponentDependencies : AppDependencies {

    val stringResources: StringResources

    val retrofit: Retrofit
}