package com.hotels.feature.details.impl.presentation

import com.hotels.core.ui.R as CoreRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hotels.core.network.response.ApiResponse
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.core.utils.liveData.SingleLiveEvent
import com.hotels.feature.details.impl.domain.entity.HotelDetails
import com.hotels.feature.details.impl.domain.useCase.HotelDetailsUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch

class HotelDetailsViewModel @AssistedInject constructor(
    @Assisted private val hotelId: Long,
    private val hotelDetailsUseCase: HotelDetailsUseCase,
    private val stringResources: StringResources
) : ViewModel() {

    val hotelDetails: LiveData<HotelDetails?> get() = _hotelDetails
    private val _hotelDetails = MutableLiveData<HotelDetails?>()
    val exceptionText: LiveData<String> get() = _exceptionText
    private val _exceptionText = SingleLiveEvent<String>()
    val progressBarVisibility: LiveData<Boolean> get() = _progressBarVisibility
    private val _progressBarVisibility = MutableLiveData<Boolean>()

    init {
        getHotelDetails()
    }

    private fun getHotelDetails() {
        viewModelScope.launch {
            _progressBarVisibility.value = true
            when (val result = hotelDetailsUseCase.getHotelDetailsById(hotelId)) {
                is ApiResponse.Failure.HttpFailure -> {
                    _exceptionText.value =
                        stringResources.getString(CoreRes.string.server_exception_toast)
                }
                is ApiResponse.Failure.NetworkFailure -> {
                    _exceptionText.value =
                        stringResources.getString(CoreRes.string.network_exception_toast)
                }
                is ApiResponse.Failure.UnknownFailure -> {
                    _exceptionText.value =
                        stringResources.getString(CoreRes.string.exception_toast)
                }
                is ApiResponse.Success -> _hotelDetails.value = result.data
            }
            _progressBarVisibility.value = false
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(@Assisted hotelId: Long): HotelDetailsViewModel
    }
}