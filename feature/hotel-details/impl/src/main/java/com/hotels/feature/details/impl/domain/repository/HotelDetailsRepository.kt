package com.hotels.feature.details.impl.domain.repository

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.details.impl.domain.entity.HotelDetails

internal interface HotelDetailsRepository {

    suspend fun getHotelDetailsById(id: Long): ApiResponse<HotelDetails>
}