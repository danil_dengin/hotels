package com.hotels.feature.details.impl.data

import com.hotels.core.network.response.ApiResponse
import com.hotels.core.network.response.map
import com.hotels.feature.details.impl.domain.entity.HotelDetails
import com.hotels.feature.details.impl.domain.mapper.HotelDetailsDtoToDomainMapper
import com.hotels.feature.details.impl.domain.repository.HotelDetailsRepository
import com.hotels.feature.service.api.HotelService
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class HotelDetailsRepositoryImpl @Inject constructor(
    private val hotelService: HotelService,
    private val hotelDetailsMapper: HotelDetailsDtoToDomainMapper
) : HotelDetailsRepository {

    override suspend fun getHotelDetailsById(id: Long): ApiResponse<HotelDetails> {
        val response = hotelService.getHotelDetailsById(id)
        return withContext(Dispatchers.Default) {
            response.map(hotelDetailsMapper::map)
        }
    }
}
