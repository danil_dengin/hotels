package com.hotels.feature.details.impl.domain.useCase

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.details.impl.domain.entity.HotelDetails
import com.hotels.feature.details.impl.domain.repository.HotelDetailsRepository
import javax.inject.Inject

internal class HotelDetailsUseCaseImpl @Inject constructor(
    private val hotelDetailsRepository: HotelDetailsRepository
) : HotelDetailsUseCase {

    override suspend fun getHotelDetailsById(id: Long): ApiResponse<HotelDetails> {
        return hotelDetailsRepository.getHotelDetailsById(id)
    }
}