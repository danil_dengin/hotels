package com.hotels.feature.details.impl.domain.mapper

import com.hotels.core.di.FeatureScope
import com.hotels.core.network.mapper.buildImageUrl
import com.hotels.core.ui.R
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.core.ui.view.parseDoubleToText
import com.hotels.core.utils.delegate.unsafeLazy
import com.hotels.feature.details.impl.domain.entity.HotelDetails
import com.hotels.feature.service.dto.HotelDetailsDto
import javax.inject.Inject

@FeatureScope
internal class HotelDetailsDtoToDomainMapper @Inject constructor(
    private val stringResources: StringResources
) {

    private val averageRatingText: String by unsafeLazy {
        stringResources.getString(R.string.average_rating_text_view)
    }

    private val distanceToCenterText: String by unsafeLazy {
        stringResources.getString(R.string.distance_to_center_text_view)
    }

    private val availableRoomText: String by unsafeLazy {
        stringResources.getString(R.string.available_room_text_view)
    }

    fun map(hotelDetailsDto: HotelDetailsDto): HotelDetails {
        return with(hotelDetailsDto) {
            HotelDetails(
                id = id,
                name = name,
                address = address,
                stars = parseDoubleToText(averageRatingText, stars),
                distance = parseDoubleToText(distanceToCenterText, distance),
                imageUrl = buildImageUrl(imageUrl),
                suitesAvailability = String.format(
                    availableRoomText,
                    parseSuitesAvailability(suitesAvailability)
                )
            )
        }
    }

    private fun parseSuitesAvailability(suitesAvailability: String): String {
        val list = suitesAvailability.split(":").toMutableList().apply { remove("") }
        return list.joinToString(", ")
    }
}
