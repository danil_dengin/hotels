package com.hotels.feature.details.impl.domain.useCase

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.details.impl.domain.entity.HotelDetails

interface HotelDetailsUseCase {

    suspend fun getHotelDetailsById(id: Long): ApiResponse<HotelDetails>
}