package com.hotels.feature.details.impl.domain.entity

data class HotelDetails(
    val id: Long,
    val name: String,
    val address: String,
    val stars: String,
    val distance: String,
    val imageUrl: String?,
    val suitesAvailability: String
)