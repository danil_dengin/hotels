package com.hotels.feature.details.impl.di

import com.hotels.core.di.FeatureScope
import com.hotels.feature.details.impl.presentation.HotelDetailsFragment
import dagger.Component

@FeatureScope
@Component(
    dependencies = [HotelDetailsComponentDependencies::class],
    modules = [HotelDetailsModule::class]
)
internal interface HotelDetailsComponent {

    fun inject(hotelDetailsFragment: HotelDetailsFragment)

    @Component.Factory
    interface Factory {
        fun create(hotelListComponentDependencies: HotelDetailsComponentDependencies): HotelDetailsComponent
    }
}