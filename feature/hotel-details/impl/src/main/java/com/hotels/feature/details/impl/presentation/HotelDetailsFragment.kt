package com.hotels.feature.details.impl.presentation

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hotels.core.di.getAppDependenciesProvider
import com.hotels.core.mvvm.viewModel
import com.hotels.core.ui.glide.FrameCropGlide
import com.hotels.core.utils.delegate.unsafeLazy
import com.hotels.feature.details.impl.R
import com.hotels.feature.details.impl.databinding.FragmentHotelDetailsBinding
import com.hotels.feature.details.impl.di.DaggerHotelDetailsComponent
import com.hotels.feature.details.impl.domain.entity.HotelDetails
import javax.inject.Inject
import javax.inject.Provider

class HotelDetailsFragment : Fragment(R.layout.fragment_hotel_details) {

    @Inject
    lateinit var viewModelProviderFactory: Provider<HotelDetailsViewModel.Factory>

    private val binding by viewBinding(FragmentHotelDetailsBinding::bind)

    private val viewModel by unsafeLazy {
        viewModel {
            viewModelProviderFactory.get().create(hotelId)
        }
    }

    private val hotelId: Long by unsafeLazy { arguments?.getLong(HOTEL_ID_ARG) ?: -1 }

    override fun onAttach(context: Context) {
        DaggerHotelDetailsComponent.factory().create(requireContext().getAppDependenciesProvider())
            .inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        viewModel.hotelDetails.observe(viewLifecycleOwner, ::updateView)
        viewModel.exceptionText.observe(viewLifecycleOwner, ::showExceptionToast)
        viewModel.progressBarVisibility.observe(viewLifecycleOwner, ::showProgressBar)
    }

    private fun showProgressBar(visibility: Boolean) {
        binding.containerDetails.isVisible = visibility.not()
        binding.progressBar.isVisible = visibility
    }

    private fun showExceptionToast(exception: String) {
        Toast.makeText(requireContext(), exception, Toast.LENGTH_LONG).show()
        binding.containerView.isVisible = false
    }

    private fun updateView(hotelDetails: HotelDetails?) {
        with(binding) {
            hotelDetails?.also { hotel ->
                hotelAddressTextView.text = hotel.address
                hotelDistanceTextView.text = hotel.distance
                hotelStarsTextView.text = hotel.stars
                hotelAvailableRoomTextView.text = hotel.suitesAvailability
                if (!hotel.imageUrl.isNullOrBlank()) {
                    setHotelImage(hotelDetails)
                } else {
                    binding.hotelImage.isVisible = false
                }
            }
        }
    }

    private fun setHotelImage(hotelDetails: HotelDetails) {
        Glide.with(requireContext())
            .asBitmap()
            .load(hotelDetails.imageUrl)
            .transform(FrameCropGlide())
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap, transition: Transition<in Bitmap>?
                ) {
                    if (resource.height > ALLOWED_PIXELS_NUMBER_HEIGHT &&
                        resource.width > ALLOWED_PIXELS_NUMBER_WIDTH
                    ) {
                        scaleBitmap(resource)
                    }
                }

                override fun onLoadCleared(placeholder: Drawable?) = Unit
            })
    }

    private fun scaleBitmap(resource: Bitmap) {
        val widthPhoneScreen =
            requireContext().resources.displayMetrics.widthPixels
        val widthMarginHotelImage =
            binding.hotelImage.marginLeft + binding.hotelImage.marginRight
        val widthHotelImage = widthPhoneScreen - widthMarginHotelImage
        val ratio = widthHotelImage.toFloat() / resource.width.toFloat()
        binding.hotelImage.setImageBitmap(
            Bitmap.createScaledBitmap(
                resource,
                widthHotelImage,
                (resource.height * ratio).toInt(),
                false
            )
        )
    }

    companion object {
        const val ALLOWED_PIXELS_NUMBER_WIDTH = 500
        const val ALLOWED_PIXELS_NUMBER_HEIGHT = 350
        const val HOTEL_ID_ARG = "hotelId"
        const val HOTEL_NAME_ARG = "hotelName"
    }
}