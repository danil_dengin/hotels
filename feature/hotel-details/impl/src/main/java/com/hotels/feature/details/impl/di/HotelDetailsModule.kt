package com.hotels.feature.details.impl.di

import com.hotels.core.di.FeatureScope
import com.hotels.feature.details.impl.data.HotelDetailsRepositoryImpl
import com.hotels.feature.details.impl.domain.repository.HotelDetailsRepository
import com.hotels.feature.details.impl.domain.useCase.HotelDetailsUseCase
import com.hotels.feature.details.impl.domain.useCase.HotelDetailsUseCaseImpl
import com.hotels.feature.service.api.HotelService
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
internal interface HotelDetailsModule {

    @FeatureScope
    @Binds
    fun bindHotelDetailsRepository(hotelDetailsRepositoryImpl: HotelDetailsRepositoryImpl): HotelDetailsRepository

    @FeatureScope
    @Binds
    fun bindHotelDetailsUseCase(hotelDetailsUseCaseImpl: HotelDetailsUseCaseImpl): HotelDetailsUseCase

    companion object {
        @FeatureScope
        @Provides
        fun provideHotelService(retrofit: Retrofit): HotelService {
            return retrofit.create(HotelService::class.java)
        }
    }
}