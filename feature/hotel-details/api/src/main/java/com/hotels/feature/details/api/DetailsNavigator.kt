package com.hotels.feature.details.api

import androidx.navigation.NavController

interface DetailsNavigator {

    fun navigateToHotelDetails(
        hotelId: Long,
        hotelName: String,
        navController: NavController
    )
}