package com.hotels.feature.service.api

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.service.dto.HotelDetailsDto
import com.hotels.feature.service.dto.HotelListDto
import retrofit2.http.GET
import retrofit2.http.Path

interface HotelService {

    @GET("0777.json")
    suspend fun getHotelsList(): ApiResponse<List<HotelListDto>?>

    @GET("{id}.json")
    suspend fun getHotelDetailsById(
        @Path("id") hotelId: Long
    ): ApiResponse<HotelDetailsDto>

}