package com.hotels.feature.service.dto

import com.google.gson.annotations.SerializedName

data class HotelDetailsDto(

    @SerializedName("id")
    val id: Long,

    @SerializedName("name")
    val name: String,

    @SerializedName("address")
    val address: String,

    @SerializedName("stars")
    val stars: Double,

    @SerializedName("distance")
    val distance: Double,

    @SerializedName("image")
    val imageUrl: String?,

    @SerializedName("suites_availability")
    val suitesAvailability: String
)