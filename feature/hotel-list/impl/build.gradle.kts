plugins {
    id(Plugins.androidLibrary)
    id(Plugins.kotlinKapt)
    id(Plugins.jetbrainsAndroid)
    id(Plugins.junit) version Versions.junitPlugin
}

applyAndroid(useViewBinding = true)

dependencies {
    implementation(project(":core:ui"))
    implementation(project(":core:utils"))
    implementation(project(":core:network"))
    implementation(project(":core:mvvm"))
    implementation(project(":core:di"))
    implementation(project(":feature:hotel-details:api"))
    implementation(project(":feature:hotel-filter:api"))
    implementation(project(":feature:hotel-service"))
    implementation(Dependencies.appCompat)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.viewBindingDelegate)
    implementation(Dependencies.lifecycleViewModel)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.material)
    implementation(Dependencies.recyclerView)
    retrofit()
    dagger()
    jetpackNavigation()
    implementation(Dependencies.viewBindingDelegate)
    implementation(Dependencies.cardView)
    testImplementation(Dependencies.logger)
    testImplementation(Dependencies.junitApi)
    testRuntimeOnly(Dependencies.junitEngine)
    testImplementation(Dependencies.mockk)
}