package com.hotels.feature.list.impl.domain.useCase

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.list.impl.domain.entity.HotelList

internal interface HotelListUseCase {

    suspend fun getHotelList(): ApiResponse<List<HotelList>?>

    suspend fun filterHotelsByDistance(): List<HotelList>

    suspend fun filterHotelsByAvailableRoom(): List<HotelList>

    fun resetFilter(): List<HotelList>
}