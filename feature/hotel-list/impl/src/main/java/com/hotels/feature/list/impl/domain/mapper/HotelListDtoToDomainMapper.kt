package com.hotels.feature.list.impl.domain.mapper

import com.hotels.core.di.FeatureScope
import com.hotels.core.ui.R
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.core.ui.view.parseDoubleToText
import com.hotels.core.utils.delegate.unsafeLazy
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.service.dto.HotelListDto
import javax.inject.Inject

@FeatureScope
internal class HotelListDtoToDomainMapper @Inject constructor(
    private val stringResources: StringResources
) {

    private val averageRatingText: String by unsafeLazy {
        stringResources.getString(R.string.average_rating_text_view)
    }

    private val distanceToCenterText: String by unsafeLazy {
        stringResources.getString(R.string.distance_to_center_text_view)
    }

    fun map(hotelListDto: HotelListDto): HotelList {
        return with(hotelListDto) {
            HotelList(
                id = id,
                name = name,
                stars = parseDoubleToText(averageRatingText, stars),
                distance = distance,
                distanceText = parseDoubleToText(distanceToCenterText, distance),
                availableRoomsNumber = getAvailableRoomsNumber(suitesAvailability),
                availableRoomsNumberText = getAvailableRoomsNumberText(suitesAvailability)
            )
        }
    }

    private fun getAvailableRoomsNumberText(suitesAvailability: String): String {
        val list = parseSuitesAvailability(suitesAvailability)
        return stringResources.getQuantityString(
            R.plurals.available_room_text_view,
            list.size,
            list.size
        )
    }

    private fun getAvailableRoomsNumber(suitesAvailability: String): Int {
        return parseSuitesAvailability(suitesAvailability).size
    }

    private fun parseSuitesAvailability(suitesAvailability: String): List<String> {
        return suitesAvailability.split(":").toMutableList().apply { remove("") }
    }
}
