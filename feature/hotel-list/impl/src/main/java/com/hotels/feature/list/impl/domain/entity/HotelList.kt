package com.hotels.feature.list.impl.domain.entity

internal data class HotelList(
    val id: Long,
    val name: String,
    val stars: String,
    val distance: Double,
    val distanceText: String,
    val availableRoomsNumber: Int,
    val availableRoomsNumberText: String
)