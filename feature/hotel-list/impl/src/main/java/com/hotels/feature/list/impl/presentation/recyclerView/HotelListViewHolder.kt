package com.hotels.feature.list.impl.presentation.recyclerView

import androidx.recyclerview.widget.RecyclerView
import com.hotels.feature.list.impl.databinding.HotelItemBinding
import com.hotels.feature.list.impl.domain.entity.HotelList


internal class HotelListViewHolder(
    private val binding: HotelItemBinding,
    navigateToHotelDetailsById: (Long, String) -> Unit,
) : RecyclerView.ViewHolder(binding.root) {

    private var hotelId: Long? = null

    init {
        itemView.setOnClickListener {
            if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                hotelId?.also { id ->
                    navigateToHotelDetailsById(id, binding.hotelNameTextView.text.toString())
                }
            }
        }
    }

    fun bind(hotelList: HotelList) {
        with(binding) {
            hotelNameTextView.text = hotelList.name
            hotelDistanceTextView.text = hotelList.distanceText
            hotelStarsTextView.text = hotelList.stars
            availableRoomTextView.text = hotelList.availableRoomsNumberText
            hotelId = hotelList.id
        }
    }
}