package com.hotels.feature.list.impl.di

import com.hotels.core.di.FeatureScope
import com.hotels.feature.list.impl.presentation.HotelListFragment
import dagger.Component

@FeatureScope
@Component(
    dependencies = [HotelListComponentDependencies::class],
    modules = [HotelListModule::class]
)
internal interface HotelListComponent {

    fun inject(hotelListFragment: HotelListFragment)

    @Component.Factory
    interface Factory {
        fun create(hotelListComponentDependencies: HotelListComponentDependencies): HotelListComponent
    }
}