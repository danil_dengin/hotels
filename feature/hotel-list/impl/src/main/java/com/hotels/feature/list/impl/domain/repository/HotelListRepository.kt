package com.hotels.feature.list.impl.domain.repository

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.list.impl.domain.entity.HotelList

internal interface HotelListRepository {

    suspend fun getHotelList(): ApiResponse<List<HotelList>?>
}