package com.hotels.feature.list.impl.domain.useCase

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.list.impl.domain.repository.HotelListRepository
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class HotelListUseCaseImpl @Inject constructor(
    private val hotelListRepository: HotelListRepository
) : HotelListUseCase {

    private var hotels = emptyList<HotelList>()

    override suspend fun getHotelList(): ApiResponse<List<HotelList>?> {
        return hotelListRepository.getHotelList().also { apiResponse ->
            if (apiResponse is ApiResponse.Success) {
                apiResponse.data?.also { data ->
                    hotels = data
                }
            }
        }
    }

    override suspend fun filterHotelsByDistance(): List<HotelList> {
        return withContext(Dispatchers.Default) {
            hotels.sortedBy { hotel ->
                hotel.distance
            }
        }
    }

    override fun resetFilter(): List<HotelList> {
        return hotels
    }

    override suspend fun filterHotelsByAvailableRoom(): List<HotelList> {
        return withContext(Dispatchers.Default) {
            hotels
                .sortedBy { hotel -> hotel.availableRoomsNumber }
                .reversed()
        }
    }
}