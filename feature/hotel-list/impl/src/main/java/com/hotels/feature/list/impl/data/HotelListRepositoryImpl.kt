package com.hotels.feature.list.impl.data

import com.hotels.core.network.response.ApiResponse
import com.hotels.core.network.response.map
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.list.impl.domain.mapper.HotelListDtoToDomainMapper
import com.hotels.feature.list.impl.domain.repository.HotelListRepository
import com.hotels.feature.service.api.HotelService
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class HotelListRepositoryImpl @Inject constructor(
    private val hotelService: HotelService,
    private val hotelListMapper: HotelListDtoToDomainMapper
) : HotelListRepository {

    override suspend fun getHotelList(): ApiResponse<List<HotelList>?> {
        val response = hotelService.getHotelsList()
        return withContext(Dispatchers.Default) {
            response.map { list ->
                list?.map(hotelListMapper::map)
            }
        }
    }
}