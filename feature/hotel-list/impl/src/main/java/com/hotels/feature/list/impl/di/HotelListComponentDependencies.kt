package com.hotels.feature.list.impl.di

import com.hotels.core.di.AppDependencies
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.feature.details.api.DetailsNavigator
import com.hotels.feature.filter.api.FilterNavigator
import retrofit2.Retrofit

interface HotelListComponentDependencies : AppDependencies {

    val stringResources: StringResources

    val detailsNavigator: DetailsNavigator

    val filterNavigator: FilterNavigator

    val retrofit: Retrofit
}