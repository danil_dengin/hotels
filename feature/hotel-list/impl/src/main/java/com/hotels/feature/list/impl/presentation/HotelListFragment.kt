package com.hotels.feature.list.impl.presentation

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.hotels.core.di.getAppDependenciesProvider
import com.hotels.core.mvvm.viewModel
import com.hotels.core.ui.recyclerView.VerticalItemDecorator
import com.hotels.core.utils.delegate.unsafeLazy
import com.hotels.feature.filter.api.FILTER_BUNDLE_KEY
import com.hotels.feature.filter.api.FILTER_KEY
import com.hotels.feature.filter.api.SelectedFilter
import com.hotels.feature.list.impl.R
import com.hotels.feature.list.impl.databinding.FragmentHotelListBinding
import com.hotels.feature.list.impl.di.DaggerHotelListComponent
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.list.impl.presentation.recyclerView.HotelListAdapter
import javax.inject.Inject
import javax.inject.Provider

internal class HotelListFragment : Fragment(R.layout.fragment_hotel_list) {

    @Inject
    lateinit var viewModelProvider: Provider<HotelListViewModel>

    private val binding by viewBinding(FragmentHotelListBinding::bind)

    private val viewModel by unsafeLazy { viewModel { viewModelProvider.get() } }

    private val hotelListAdapter by unsafeLazy { HotelListAdapter(::navigateToHotelDetails) }

    override fun onAttach(context: Context) {
        DaggerHotelListComponent.factory().create(requireContext().getAppDependenciesProvider())
            .inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initMenu()
        initFragmentResultListener()
        initObservers()
    }

    private fun initObservers() {
        viewModel.hotelList.observe(viewLifecycleOwner, ::updateView)
        viewModel.progressBarVisibility.observe(viewLifecycleOwner, ::showProgressBar)
        viewModel.exceptionText.observe(viewLifecycleOwner, ::showExceptionToast)
    }

    private fun showProgressBar(visibility: Boolean) {
        binding.progressBar.isVisible = visibility
    }

    private fun updateView(hotels: List<HotelList>?) {
        hotelListAdapter.submitList(hotels) {
            if (viewModel.isNeededScrollToTop) {
                binding.hotelListRecyclerView.scrollToPosition(0)
                viewModel.isNeededScrollToTop = false
            }
        }
    }

    private fun showExceptionToast(exception: String) {
        Toast.makeText(requireContext(), exception, Toast.LENGTH_LONG).show()
        binding.containerView.isVisible = false
    }

    private fun initFragmentResultListener() {
        parentFragmentManager.setFragmentResultListener(
            FILTER_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val filter = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                bundle.getParcelable(FILTER_BUNDLE_KEY, SelectedFilter::class.java)
            } else {
                bundle.getParcelable(FILTER_BUNDLE_KEY)
            }
            if (filter != null) {
                viewModel.isNeededScrollToTop = true
                when (filter) {
                    SelectedFilter.NOTHING -> {
                        viewModel.selectedFilter = SelectedFilter.NOTHING
                        viewModel.resetFilter()
                    }
                    SelectedFilter.AVAILABLE_ROOM -> {
                        viewModel.selectedFilter = SelectedFilter.AVAILABLE_ROOM
                        viewModel.filterHotelsByAvailableRoom()
                    }
                    SelectedFilter.DISTANCE -> {
                        viewModel.selectedFilter = SelectedFilter.DISTANCE
                        viewModel.filterHotelsByDistance()
                    }
                }
            }
        }
    }

    private fun initMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.hotel_list_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return if (menuItem.itemId == R.id.filterView) {
                    navigateToHotelFilter()
                    return true
                } else {
                    false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.STARTED)
    }

    private fun initRecyclerView() {
        with(binding.hotelListRecyclerView) {
            adapter = hotelListAdapter
            addItemDecoration(VerticalItemDecorator())
        }
    }

    private fun navigateToHotelDetails(hotelId: Long, hotelName: String) {
        viewModel.navigateToHotelDetails(hotelId, hotelName, findNavController())
    }

    private fun navigateToHotelFilter() {
        viewModel.navigateToHotelFilter(findNavController())
    }
}
