package com.hotels.feature.list.impl.di

import com.hotels.core.di.FeatureScope
import com.hotels.feature.list.impl.data.HotelListRepositoryImpl
import com.hotels.feature.list.impl.domain.repository.HotelListRepository
import com.hotels.feature.list.impl.domain.useCase.HotelListUseCase
import com.hotels.feature.list.impl.domain.useCase.HotelListUseCaseImpl
import com.hotels.feature.service.api.HotelService
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
internal interface HotelListModule {

    @FeatureScope
    @Binds
    fun bindHotelListRepository(hotelListRepositoryImpl: HotelListRepositoryImpl): HotelListRepository

    @FeatureScope
    @Binds
    fun bindHotelListUseCase(hotelListUseCaseImpl: HotelListUseCaseImpl): HotelListUseCase

    companion object {
        @FeatureScope
        @Provides
        fun provideHotelService(retrofit: Retrofit): HotelService {
            return retrofit.create(HotelService::class.java)
        }
    }
}