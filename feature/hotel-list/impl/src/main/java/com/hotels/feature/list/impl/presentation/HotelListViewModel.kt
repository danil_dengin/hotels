package com.hotels.feature.list.impl.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.hotels.core.network.response.ApiResponse
import com.hotels.core.ui.R
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.core.utils.liveData.SingleLiveEvent
import com.hotels.core.utils.tag.tagObj
import com.hotels.feature.details.api.DetailsNavigator
import com.hotels.feature.filter.api.FilterNavigator
import com.hotels.feature.filter.api.SelectedFilter
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.list.impl.domain.useCase.HotelListUseCase
import javax.inject.Inject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

internal class HotelListViewModel @Inject constructor(
    private val detailsNavigator: DetailsNavigator,
    private val filterNavigator: FilterNavigator,
    private val hotelListUseCase: HotelListUseCase,
    private val stringResources: StringResources
) : ViewModel() {

    val hotelList: LiveData<List<HotelList>?> get() = _hotelsList
    val exceptionText: LiveData<String> get() = _exceptionText
    val progressBarVisibility: LiveData<Boolean> get() = _progressBarVisibility
    var isNeededScrollToTop = false
    var selectedFilter = SelectedFilter.NOTHING
    private val _exceptionText = SingleLiveEvent<String>()
    private val _hotelsList = MutableLiveData<List<HotelList>?>()
    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e(HOTEL_LIST_VIEW_MODEL_TAG, throwable.toString())
        _exceptionText.value = stringResources.getString(R.string.exception_toast)
    }
    private val _progressBarVisibility = MutableLiveData<Boolean>()

    init {
        getHotelsList()
    }

    private fun getHotelsList() {
        viewModelScope.launch {
            _progressBarVisibility.value = true
            when (val result = hotelListUseCase.getHotelList()) {
                is ApiResponse.Failure.HttpFailure -> {
                    _exceptionText.value =
                        stringResources.getString(R.string.server_exception_toast)
                }
                is ApiResponse.Failure.NetworkFailure -> {
                    _exceptionText.value =
                        stringResources.getString(R.string.network_exception_toast)
                }
                is ApiResponse.Failure.UnknownFailure -> {
                    _exceptionText.value =
                        stringResources.getString(R.string.exception_toast)
                }
                is ApiResponse.Success -> _hotelsList.value = result.data
            }
            _progressBarVisibility.value = false
        }
    }

    fun navigateToHotelDetails(hotelId: Long, hotelName: String, navController: NavController) {
        detailsNavigator.navigateToHotelDetails(hotelId, hotelName, navController)
    }

    fun navigateToHotelFilter(navController: NavController) {
        filterNavigator.navigateToHotelFilter(selectedFilter, navController)
    }

    fun filterHotelsByDistance() {
        viewModelScope.launch(coroutineExceptionHandler) {
            _hotelsList.value = hotelListUseCase.filterHotelsByDistance()
        }
    }

    fun filterHotelsByAvailableRoom() {
        viewModelScope.launch(coroutineExceptionHandler) {
            _hotelsList.value = hotelListUseCase.filterHotelsByAvailableRoom()
        }
    }

    fun resetFilter() {
        _hotelsList.value = hotelListUseCase.resetFilter()
    }

    private companion object {
        val HOTEL_LIST_VIEW_MODEL_TAG: String = HotelListViewModel.tagObj()
    }
}