package com.hotels.feature.list.impl.presentation.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.hotels.feature.list.impl.databinding.HotelItemBinding
import com.hotels.feature.list.impl.domain.entity.HotelList

internal class HotelListAdapter(
    private val navigateToHotelDetailsById: (Long, String) -> Unit,
) : ListAdapter<HotelList, HotelListViewHolder>(DiffCallback()) {

    private class DiffCallback : DiffUtil.ItemCallback<HotelList>() {

        override fun areItemsTheSame(oldContact: HotelList, newContact: HotelList) =
            oldContact.id == newContact.id

        override fun areContentsTheSame(oldContact: HotelList, newContact: HotelList) =
            oldContact == newContact
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelListViewHolder {
        val binding = HotelItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HotelListViewHolder(binding, navigateToHotelDetailsById)
    }

    override fun onBindViewHolder(holder: HotelListViewHolder, position: Int) {
        holder.bind(hotelList = currentList[position])
    }
}
