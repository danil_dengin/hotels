package com.hotels.feature.list.impl.domain

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.list.impl.domain.entity.HotelList

internal val hotel1 = HotelList(
    id = 1,
    name = "Hotel1",
    stars = "2",
    distance = 10.0,
    distanceText = "10 km from the center of city",
    availableRoomsNumber = 8,
    availableRoomsNumberText = "8 available rooms"
)

internal val hotel2 = HotelList(
    id = 2,
    name = "Hotel2",
    stars = "4",
    distance = 10.3,
    distanceText = "10.3 km from the center of city",
    availableRoomsNumber = 1,
    availableRoomsNumberText = "1 available room"
)

internal val hotel3 = HotelList(
    id = 3,
    name = "Hotel3",
    stars = "3",
    distance = 4343.0,
    distanceText = "4343 km from the center of city",
    availableRoomsNumber = 0,
    availableRoomsNumberText = "There're no available rooms"
)

internal val hotel4 = HotelList(
    id = 4,
    name = "Hotel4",
    stars = "5",
    distance = 12.3,
    distanceText = "12.3 km from the center of city",
    availableRoomsNumber = 4,
    availableRoomsNumberText = "4 available rooms"
)

internal val hotelsServiceResponseTest = ApiResponse.Success(listOf(hotel1, hotel2, hotel3, hotel4))

internal val hotelsFilteredByDistanceTest = listOf(hotel1, hotel2, hotel4, hotel3)

internal val hotelsFilteredByRoomTest = listOf(hotel1, hotel4, hotel2, hotel3)