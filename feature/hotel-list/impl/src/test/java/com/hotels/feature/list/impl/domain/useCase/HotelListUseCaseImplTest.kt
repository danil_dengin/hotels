package com.hotels.feature.list.impl.domain.useCase

import com.hotels.core.network.response.ApiResponse
import com.hotels.feature.list.impl.domain.entity.HotelList
import com.hotels.feature.list.impl.domain.hotelsFilteredByDistanceTest
import com.hotels.feature.list.impl.domain.hotelsFilteredByRoomTest
import com.hotels.feature.list.impl.domain.hotelsServiceResponseTest
import com.hotels.feature.list.impl.domain.hotelsTest
import com.hotels.feature.list.impl.domain.repository.HotelListRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class HotelListUseCaseImplTest {

    private val hotelListRepository = mockk<HotelListRepository>()

    private lateinit var hotelListUseCaseImpl: HotelListUseCase

    private var actualServerResponse = ApiResponse.Success(emptyList<HotelList>())

    private var actual = emptyList<HotelList>()

    @BeforeEach
    fun setUp() {
        actual = emptyList<HotelList>()
        actualServerResponse = ApiResponse.Success(emptyList<HotelList>())
        hotelListUseCaseImpl = HotelListUseCaseImpl(hotelListRepository)
    }

    @Test
    fun `should return the same list of hotels as in repository`() = runBlocking {
        coEvery { hotelListRepository.getHotelList() } returns hotelsServiceResponseTest
        actualServerResponse =
            hotelListUseCaseImpl.getHotelList() as ApiResponse.Success<List<HotelList>>
        coVerify(exactly = 1) { hotelListRepository.getHotelList() }
        val expected = hotelsServiceResponseTest
        assertEquals(expected, actualServerResponse)
    }

    @Test
    fun `should return filtered list by distance`() = runBlocking {
        coEvery { hotelListRepository.getHotelList() } returns hotelsServiceResponseTest
        hotelListUseCaseImpl.getHotelList()
        actual = hotelListUseCaseImpl.filterHotelsByDistance()
        val expected = hotelsFilteredByDistanceTest
        assertEquals(actual, expected)
    }

    @Test
    fun `should return filtered list by number of available rooms`() = runBlocking {
        coEvery { hotelListRepository.getHotelList() } returns hotelsServiceResponseTest
        hotelListUseCaseImpl.getHotelList()
        actual = hotelListUseCaseImpl.filterHotelsByAvailableRoom()
        val expected = hotelsFilteredByRoomTest
        assertEquals(actual, expected)
    }
}