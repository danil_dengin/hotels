object AppConfig {
    const val versionCode = 1
    const val versionName = "1.0"
    const val targetSdkVersion = 33
    const val minSdkVersion = 26
    const val compileSdkVersion = 33
    const val applicationId = "com.hotels"
    const val javaVersion = "11"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}