import org.gradle.kotlin.dsl.DependencyHandlerScope

fun DependencyHandlerScope.dagger() {
    implementation(Dependencies.dagger)
    kapt(Dependencies.daggerKapt)
}

fun DependencyHandlerScope.jetpackNavigation() {
    implementation(Dependencies.navigationUi)
    implementation(Dependencies.navigationFragment)
}

fun DependencyHandlerScope.retrofit() {
    implementation(Dependencies.retrofit)
    implementation(Dependencies.gsonRetrofitConvertor)
    implementation(Dependencies.http)
    implementation(Dependencies.httpLoggingInterceptor)
}

fun DependencyHandlerScope.coroutines() {
    implementation(Dependencies.coroutines)
    implementation(Dependencies.coroutinesCore)
}

private fun DependencyHandlerScope.implementation(name: String) {
    add("implementation", name)
}

private fun DependencyHandlerScope.kapt(name: String) {
    add("kapt", name)
}

private fun DependencyHandlerScope.annotationProcessor(name: String) {
    add("annotationProcessor", name)
}