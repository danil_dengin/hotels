package com.hotels.di

import android.content.Context
import com.hotels.core.ui.stringResources.StringResources
import com.hotels.feature.details.api.DetailsNavigator
import com.hotels.feature.details.impl.di.HotelDetailsComponentDependencies
import com.hotels.feature.filter.api.FilterNavigator
import com.hotels.feature.list.impl.di.HotelListComponentDependencies
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton
import retrofit2.Retrofit

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        NavigationModule::class,
        StringResourcesBindingModule::class
    ]
)
internal interface AppComponent : HotelListComponentDependencies,
    HotelDetailsComponentDependencies {

    override val retrofit: Retrofit

    override val stringResources: StringResources

    override val detailsNavigator: DetailsNavigator

    override val filterNavigator: FilterNavigator

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context
        ): AppComponent
    }
}