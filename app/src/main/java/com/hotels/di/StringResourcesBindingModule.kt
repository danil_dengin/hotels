package com.hotels.di

import com.hotels.core.ui.stringResources.StringResources
import com.hotels.core.ui.stringResources.StringResourcesImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface StringResourcesBindingModule {

    @Binds
    @Singleton
    fun bindStringResources(stringResourcesImpl: StringResourcesImpl): StringResources
}