package com.hotels.di

import androidx.navigation.ui.R as NavRes
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.navOptions
import com.hotels.R
import com.hotels.feature.details.api.DetailsNavigator
import com.hotels.feature.details.impl.presentation.HotelDetailsFragment
import com.hotels.feature.filter.api.FilterNavigator
import com.hotels.feature.filter.api.SelectedFilter
import com.hotels.feature.filter.impl.presentation.HotelFilterFragment
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal object NavigationModule {

    @Singleton
    @Provides
    fun provideHotelDetailsFragment(): DetailsNavigator {
        return object : DetailsNavigator {
            override fun navigateToHotelDetails(
                hotelId: Long,
                hotelName: String,
                navController: NavController
            ) {
                navController.navigate(
                    R.id.action_hotelListFragment_to_hotelDetailsFragment,
                    bundleOf(
                        HotelDetailsFragment.HOTEL_ID_ARG to hotelId,
                        HotelDetailsFragment.HOTEL_NAME_ARG to hotelName
                    ),
                    navOptions {
                        anim {
                            enter = NavRes.anim.nav_default_enter_anim
                            popEnter = NavRes.anim.nav_default_pop_enter_anim
                            popExit = NavRes.anim.nav_default_pop_exit_anim
                            exit = NavRes.anim.nav_default_exit_anim
                        }
                    }
                )
            }
        }
    }

    @Singleton
    @Provides
    fun provideHotelFilterFragment(): FilterNavigator {
        return object : FilterNavigator {
            override fun navigateToHotelFilter(
                selectedFilter: SelectedFilter,
                navController: NavController
            ) {
                navController.navigate(
                    R.id.action_hotelListFragment_to_hotelFilterFragment,
                    bundleOf(HotelFilterFragment.SELECTED_FILTER_ARG to selectedFilter)
                )
            }
        }
    }
}