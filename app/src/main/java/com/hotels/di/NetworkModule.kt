package com.hotels.di

import com.hotels.core.network.response.ApiResponseFactoryProvider
import com.hotels.core.network.retrofit.OkHttpClientProvider
import com.hotels.core.network.retrofit.RetrofitProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Retrofit

@Module
internal object NetworkModule {

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient = OkHttpClientProvider.get()

    @Singleton
    @Provides
    fun provideApiResponseFactory(): CallAdapter.Factory = ApiResponseFactoryProvider.get()

    @Singleton
    @Provides
    fun provideDataHotelRetrofit(
        okHttpClient: OkHttpClient,
        @HotelUrlQualifier dataUrl: String,
        apiResponseFactory: CallAdapter.Factory,
    ): Retrofit = RetrofitProvider.get(okHttpClient, dataUrl, apiResponseFactory)

    @Provides
    @HotelUrlQualifier
    fun provideDataUrl() = "https://raw.githubusercontent.com/iMofas/ios-android-test/master/"
}