package com.hotels

import android.app.Application
import com.hotels.core.di.AppDependencies
import com.hotels.core.di.AppDependenciesProvider
import com.hotels.di.AppComponent
import com.hotels.di.DaggerAppComponent

internal class App : Application(), AppDependenciesProvider {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory()
            .create(this)
    }

    override fun getDependencies(): AppDependencies {
        return appComponent
    }
}