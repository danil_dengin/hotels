plugins {
    id(Plugins.application)
    id(Plugins.kotlinKapt)
    id(Plugins.kotlinAndroid)
    id(Plugins.jetbrainsAndroid)
}

android {
    compileSdk = AppConfig.compileSdkVersion
    defaultConfig {
        applicationId = AppConfig.applicationId
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.targetSdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        testInstrumentationRunner = AppConfig.testInstrumentationRunner
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = AppConfig.javaVersion
    }
    packagingOptions {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
    implementation(project(":core:mvvm"))
    implementation(project(":core:utils"))
    implementation(project(":core:ui"))
    implementation(project(":core:network"))
    implementation(project(":core:di"))
    implementation(project(":feature:hotel-details:api"))
    implementation(project(":feature:hotel-details:impl"))
    implementation(project(":feature:hotel-list:impl"))
    implementation(project(":feature:hotel-service"))
    implementation(project(":feature:hotel-filter:api"))
    implementation(project(":feature:hotel-filter:impl"))
    implementation(Dependencies.appCompat)
    retrofit()
    jetpackNavigation()
    dagger()
    implementation(Dependencies.material)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.activityKtx)
}