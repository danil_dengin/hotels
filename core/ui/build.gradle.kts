plugins {
    id(Plugins.androidLibrary)
    id(Plugins.jetbrainsAndroid)
    id(Plugins.kotlinKapt)
}

applyAndroid()

dependencies {
    implementation(project(":core:utils"))
    dagger()
    implementation(Dependencies.appCompat)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.material)
    implementation(Dependencies.glide)
}