package com.hotels.core.ui.stringResources

interface StringResources {

    fun getString(stringId: Int): String

    fun getQuantityString(stringId: Int, quantity: Int, formatArgs: Any): String
}