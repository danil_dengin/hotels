package com.hotels.core.ui.glide

import android.graphics.Bitmap
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import java.security.MessageDigest

class FrameCropGlide : BitmapTransformation() {

    override fun transform(
        pool: BitmapPool,
        toTransform: Bitmap,
        outWidth: Int,
        outHeight: Int
    ): Bitmap = Bitmap.createBitmap(
        toTransform,
        LEFT_TOP_PIXEL_TRANSLATION,
        LEFT_TOP_PIXEL_TRANSLATION,
        (toTransform.width) - RIGHT_BOTTOM_PIXELS_TRANSLATION,
        (toTransform.height) - RIGHT_BOTTOM_PIXELS_TRANSLATION
    )

    override fun updateDiskCacheKey(messageDigest: MessageDigest) = Unit

    private companion object {
        const val LEFT_TOP_PIXEL_TRANSLATION = 1
        const val RIGHT_BOTTOM_PIXELS_TRANSLATION = 2
    }
}