package com.hotels.core.ui.view

fun parseDoubleToText(format: String, string: Double) : String {
    return String.format(format, String.format("%.0f", string))
}