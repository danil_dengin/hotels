package com.hotels.core.di

import android.content.Context

fun <T> Context.getAppDependenciesProvider() =
    (applicationContext as AppDependenciesProvider).getDependencies() as T
