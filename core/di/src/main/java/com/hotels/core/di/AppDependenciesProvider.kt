package com.hotels.core.di

interface AppDependenciesProvider {

    fun getDependencies(): AppDependencies
}