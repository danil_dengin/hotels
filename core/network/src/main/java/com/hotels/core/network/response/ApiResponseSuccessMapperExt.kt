package com.hotels.core.network.response

inline fun <T, R> ApiResponse<T>.map(transform: (T) -> R): ApiResponse<R> {
    return when (this) {
        is ApiResponse.Success -> ApiResponse.Success(transform(this.data))
        is ApiResponse.Failure.HttpFailure -> this
        is ApiResponse.Failure.NetworkFailure -> this
        is ApiResponse.Failure.UnknownFailure -> this
    }
}