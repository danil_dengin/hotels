package com.hotels.core.network.mapper

fun buildImageUrl(imageUrl: String?) = if (imageUrl != null) "$BASE_URL$imageUrl" else null

const val BASE_URL = "https://github.com/iMofas/ios-android-test/raw/master/"
