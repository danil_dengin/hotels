package com.hotels.core.network.response

object ApiResponseFactoryProvider {
    fun get() = ApiResponseCallAdapterFactory()
}