package com.hotels.core.utils.tag

fun Any.tagObj(): String = this::class.java.simpleName.takeIf { it.isNotEmpty() } ?: "TAG"