package com.hotels.core.utils.exception

enum class Exception {
    SERVER_EXCEPTION,
    NETWORK_EXCEPTION,
    FATAL_EXCEPTION
}